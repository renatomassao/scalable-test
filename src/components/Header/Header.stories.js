import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Header from './';

storiesOf('Header', module)
  .add('Default header', () => (
    <Header>
      <h1>Default Header</h1>
    </Header>
  ))
