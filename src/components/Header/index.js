import React from 'react'
import styled from 'styled-components';
import ContentWrapper from '../ContentWrapper';

const StyledHeader = styled.header`
  box-shadow: 0 3px 3px 0 rgba(0,0,0,0.14),
              0 1px 7px 0 rgba(0,0,0,0.12),
              0 3px 1px -1px rgba(0,0,0,0.2);
  display: flex;
  font-family: Helvetica, Arial, sans-serif;
  margin-bottom: 10px;

  ${ContentWrapper} {
    padding: 10px;
  }
`;

const Header = ({children}) => (
  <StyledHeader>
    <ContentWrapper>
      {children}
    </ContentWrapper>
  </StyledHeader>
)

export default Header;
