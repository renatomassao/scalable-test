import React from 'react';
import ReactDOM from 'react-dom';
import Header from './';

it('renders Header without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Header>
      <h1>Default Header</h1>
    </Header>
    , div);
  ReactDOM.unmountComponentAtNode(div);
});

