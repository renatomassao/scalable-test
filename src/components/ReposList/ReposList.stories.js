import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import ReposList from './';
import ReposItem from './ReposItem';

storiesOf('Repositories list', module)
  .add('Repos Item', () => (
    <ReposItem
      name="Repository Name"
      description="Lorem Ipsum dolor sit amet"
      stargazers="12"
      watchers="10"
      fork="2"
    />
  ))
  .add('Complete list', () => (
    <ReposList>
      <ReposItem
        name="Repository Name"
        description="Lorem Ipsum dolor sit amet"
        stargazers="12"
        watchers="10"
        fork="2"
      />
      <ReposItem
        name="Repository Name"
        description="Lorem Ipsum dolor sit amet"
        stargazers="12"
        watchers="10"
        fork="2"
      />
      <ReposItem
        name="Repository Name"
        description="Lorem Ipsum dolor sit amet"
        stargazers="12"
        watchers="10"
        fork="2"
      />
    </ReposList>
  ))

