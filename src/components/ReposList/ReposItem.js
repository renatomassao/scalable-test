import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const StyledItem = styled.div`
  font-family: Helvetiva, Arial, sans-serif;
  flex-basis: 50%;
  flex-grow: 1;
  max-width: 50%;
  padding: 10px;

  > a {
    box-shadow: 0 3px 3px 0 rgba(0,0,0,0.14),
                0 1px 7px 0 rgba(0,0,0,0.12),
                0 3px 1px -1px rgba(0,0,0,0.2);
    color: inherit;
    display: flex;
    flex-direction: column;
    height: 100%;
    justify-content: space-between;
    padding: 10px;
    text-decoration: none;

    > div {
      display: flex;
      justify-content: space-between;
    }
  }

  h2 {
    margin: 0;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  p {
    overflow: hidden;
    text-overflow: ellipsis;
  }

  span {
    display: flex;
    flex-basis: 33.3%;
    flex-direction: column;
    text-align: center;

    > i {
      margin-bottom: 5px;
    }
  }
`;

const ReposItem = ({to, name, description, stargazers, watchers, fork}) => (
  <StyledItem>
    <Link to={to}>
      <h2>{name}</h2>
      <p>{description}</p>
      <div>
        <span>
          <i className="fa fa-star"/>
          {stargazers}
        </span>
        <span>
          <i className="fa fa-eye"/>
          {watchers}
        </span>
        <span>
          <i className="fa fa-code-branch"/>
          {fork}
        </span>
      </div>
    </Link>
  </StyledItem>
);

export default ReposItem;
