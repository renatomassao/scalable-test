import styled from 'styled-components';

const ReposList = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-wrap: wrap;
  font-family: Helvetica, Arial, sans-serif;
  margin: 0;
  padding: 0;
`;

export default ReposList;
