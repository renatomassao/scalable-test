import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import ReposList from './';
import ReposItem from './ReposItem';

it('renders ReposList without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <MemoryRouter>
      <ReposList>
        <ReposItem
          to="/"
          name="Repository Name"
          description="Lorem Ipsum dolor sit amet"
          stargazers="12"
          watchers="10"
          fork="2"
        />
        <ReposItem
          to="/"
          name="Repository Name"
          description="Lorem Ipsum dolor sit amet"
          stargazers="12"
          watchers="10"
          fork="2"
        />
        <ReposItem
          to="/"
          name="Repository Name"
          description="Lorem Ipsum dolor sit amet"
          stargazers="12"
          watchers="10"
          fork="2"
        />
      </ReposList>
    </MemoryRouter>
    , div);
  ReactDOM.unmountComponentAtNode(div);
});

