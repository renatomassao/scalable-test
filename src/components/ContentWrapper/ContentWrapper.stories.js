import React from 'react';
import { storiesOf } from '@storybook/react';
import ContentWrapper from './';

storiesOf('Content Wrapper', module)
  .add('Default wrapper', () => (
    <ContentWrapper>
      <h1>Content Wrapper</h1>
      <pre><code>
        <h2>max-width: 1280px;</h2>
        <h2>width: 90%;</h2>
      </code></pre>
    </ContentWrapper>
  ))
