import styled from 'styled-components';

const ContentWrapper = styled.div`
  margin: 0 auto;
  max-width: 1280px;
  width: 90%;
`;

export default ContentWrapper;
