import React from 'react';
import ReactDOM from 'react-dom';
import ContentWrapper from './';

it('renders ContentWrapper without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <ContentWrapper>
      <h1>Content Wrapper</h1>
      <pre><code>
        <h2>max-width: 1280px;</h2>
        <h2>width: 90%;</h2>
      </code></pre>
    </ContentWrapper>
    , div);
  ReactDOM.unmountComponentAtNode(div);
});

