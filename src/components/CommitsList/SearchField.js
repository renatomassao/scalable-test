import styled from 'styled-components';

const SearchField = styled.input.attrs({
  type: 'text',
  placeholder: 'Search commits'
})`
  border: 1px solid #ccc;
  box-sizing: border-box;
  font-family: Helvetica, Arial, sans-serif;
  height: 40px;
  padding: 0 10px;
  width: 100%;
`;

export default SearchField;
