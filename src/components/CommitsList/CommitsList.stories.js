import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CommitsList from './';
import CommitsItem from './CommitsItem';
import SearchField from './SearchField';

storiesOf('Commits list', module)
  .add('Commits Item', () => (
    <CommitsItem
      message="Lorem ipsum dolor sit amet"
      author="Github"
    />
  ))
  .add('Complete list', () => (
    <React.Fragment>
      <SearchField />
      <CommitsList>
        <CommitsItem
          message="Lorem ipsum dolor sit amet"
          author="Github"
        />
        <CommitsItem
          message="Lorem ipsum dolor sit amet"
          author="Github"
        />
        <CommitsItem
          message="Lorem ipsum dolor sit amet"
          author="Github"
        />
        <CommitsItem
          message="Lorem ipsum dolor sit amet"
          author="Github"
        />
      </CommitsList>
    </React.Fragment>
  ))

