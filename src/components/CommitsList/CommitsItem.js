import React from 'react';
import styled from 'styled-components';

const StyledItem = styled.li`
  border: 1px solid #ccc;
  font-family: Helvetica, Arial, sans-serif;
  list-style: none;
  padding: 10px;

  > div {
    font-size: 16px;
    margin: 5px 0;

    &:last-child {
      font-size: 14px;
    }
  }
`;

const CommitsItem = ({message, author}) => (
  <StyledItem>
    <div>
      <strong>Commit Message:</strong> {message}
    </div>
    <div>
      <strong>Author:</strong> {author}
    </div>
  </StyledItem>
)

export default CommitsItem;
