import styled from 'styled-components';

const ReposList = styled.ul`
  font-family: Helvetica, Arial, sans-serif;
  list-style: none;
  margin: 0;
  padding: 0;
`;

export default ReposList;
