import React from 'react';
import ReactDOM from 'react-dom';
import CommitsList from './';
import CommitsItem from './CommitsItem';
import SearchField from './SearchField';

it('renders Commits without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <React.Fragment>
      <SearchField />
      <CommitsList>
        <CommitsItem
          message="Lorem ipsum dolor sit amet"
          author="Github"
        />
        <CommitsItem
          message="Lorem ipsum dolor sit amet"
          author="Github"
        />
        <CommitsItem
          message="Lorem ipsum dolor sit amet"
          author="Github"
        />
        <CommitsItem
          message="Lorem ipsum dolor sit amet"
          author="Github"
        />
      </CommitsList>
    </React.Fragment>
    , div);
  ReactDOM.unmountComponentAtNode(div);
});

