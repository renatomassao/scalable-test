export const FETCH_REPOS = 'FETCH_REPOS';
export const SET_REPOS = 'SET_REPOS';

export const fetchRepos = user => dispatch => {
  dispatch({
    type: FETCH_REPOS
  });
  return fetch(`https://api.github.com/users/${user}/repos`)
    .then(response => response.json())
    .then(json => dispatch(setRepos(json)));
};

export const setRepos = repos => ({
  type: SET_REPOS,
  payload: repos
});
