export const FETCH_COMMITS = 'FETCH_COMMITS';
export const FILTER_COMMITS = 'FILTER_COMMITS';
export const SET_COMMITS = 'SET_COMMITS';

export const fetchCommits = (user, repo) => dispatch => {
  dispatch({
    type: FETCH_COMMITS
  });
  return fetch(`https://api.github.com/repos/${user}/${repo}/commits?per_page=20`)
    .then(response => response.json())
    .then(json => dispatch(setCommits(json)));
};

export const filterCommits = term => ({
  type: FILTER_COMMITS,
  payload: term
});

export const setCommits = commits => ({
  type: SET_COMMITS,
  payload: commits
});
