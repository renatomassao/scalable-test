import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import * as actions from './repos';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
 
describe('actions', () => {
  it('should create an action to set repositories', () => {
    const repos = [{
      title: 'title'
    }];
    const expectedAction = {
      type: actions.SET_REPOS,
      payload: repos
    };
    expect(actions.setRepos(repos)).toEqual(expectedAction);
  });
});

describe('async actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });
 
  it('creates FETCH_REPOS when fetching repos has been done', () => {
    fetchMock
      .getOnce('https://api.github.com/users/facebook/repos', { body: { repos: {} }, headers: { 'content-type': 'application/json' } });

    const expectedActions = [
      { type: actions.FETCH_REPOS },
      { type: actions.SET_REPOS, payload: { repos: {} } }
    ];
    const store = mockStore({ repos: [] });
 
    return store.dispatch(actions.fetchRepos('facebook')).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
