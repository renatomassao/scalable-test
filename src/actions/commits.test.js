import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import * as actions from './commits';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions', () => {
  it('should create an action to set commits', () => {
    const commits = [{
      title: 'title'
    }];
    const expectedAction = {
      type: actions.SET_COMMITS,
      payload: commits
    };
    expect(actions.setCommits(commits)).toEqual(expectedAction);
  });

  it('should create an action to filter commits', () => {
    const term = 'needle';
    const expectedAction = {
      type: actions.FILTER_COMMITS,
      payload: term
    };
    expect(actions.filterCommits(term)).toEqual(expectedAction);
  });
});

describe('async actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });
 
  it('creates FETCH_COMMITS when fetching commits has been done', () => {
    fetchMock
      .getOnce('https://api.github.com/repos/facebook/react/commits?per_page=20', { body: { commits: {} }, headers: { 'content-type': 'application/json' } });

    const expectedActions = [
      { type: actions.FETCH_COMMITS },
      { type: actions.SET_COMMITS, payload: { commits: {} } }
    ];
    const store = mockStore({ commits: [] });
 
    return store.dispatch(actions.fetchCommits('facebook', 'react')).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    });
  });
});
