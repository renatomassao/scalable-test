import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCommits, filterCommits } from '../actions/commits';
import ContentWrapper from '../components/ContentWrapper';
import CommitsList from '../components/CommitsList';
import CommitsItem from '../components/CommitsList/CommitsItem';
import SearchField from '../components/CommitsList/SearchField';
import Header from '../components/Header';

class Commits extends Component {
  componentWillMount() {
    const { user, repo } = this.props.match.params;
    this.props.getCommits(user, repo);
  }

  render() {
    return (
      <React.Fragment>
        <Header>
          <h1>
            Commits for {this.props.match.params.repo}
          </h1>
        </Header>
        <ContentWrapper>
          <SearchField
            onChange={this.props.handleChange}
          />
          <CommitsList>
            {this.props.commits.map( (commit, index) => (
              <CommitsItem
                key={index}
                message={commit.commit.message}
                author={commit.commit.author.name}
              />
            ))}
          </CommitsList>
        </ContentWrapper>
      </React.Fragment>
    );
  }
}

export default connect(
  ({commits}) => ({
    commits: commits.visibleCommits
  }),
  dispatch => ({
    handleChange: (e) => dispatch(filterCommits(e.target.value)),
    getCommits: (user, repo) => dispatch(fetchCommits(user, repo))
  })
)(Commits);
