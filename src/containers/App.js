import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchRepos } from '../actions/repos';
import ContentWrapper from '../components/ContentWrapper';
import ReposList from '../components/ReposList';
import ReposItem from '../components/ReposList/ReposItem';
import Header from '../components/Header';

class App extends Component {
  componentWillMount() {
    this.props.getRepos('facebook');
  }

  render() {
    const { repos } = this.props;
    return (
      <React.Fragment>
        <Header>
          <h1>
            Repositories
          </h1>
        </Header>
        <ContentWrapper>
          <ReposList>
            {repos.map(repo => (
              <ReposItem
                to={`/facebook/${repo.name}`}
                key={repo.id}
                name={repo.name}
                description={repo.description}
                stargazers={repo.stargazers_count}
                watchers={repo.watchers}
                fork={repo.forks}
              />
            ))}
          </ReposList>
        </ContentWrapper>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    repos: state.repos
  }),
  dispatch => ({
    getRepos: user => dispatch(fetchRepos(user))
  })
)(App);
