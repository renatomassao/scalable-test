import {
  SET_REPOS,
} from '../actions/repos';

function repos(state = [], action) {
  switch(action.type) {
    case SET_REPOS:
      return action.payload;
    default:
      return state;
  }
}

export default repos;
