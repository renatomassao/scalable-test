import {
  SET_COMMITS,
  FILTER_COMMITS
} from '../actions/commits';

const initialState = {
  commits: [],
  visibleCommits: []
}

function commits(state = initialState, action) {
  switch(action.type) {
    case SET_COMMITS:
      return ({
        commits: [
          ...state.commits,
          ...action.payload
        ],
        visibleCommits: action.payload
      });
    case FILTER_COMMITS:
      return ({
        ...state,
        visibleCommits: state.commits.filter(
          x => x.commit.message.toLowerCase().includes(
            action.payload.toLowerCase()
          )
        ),
      })
    default:
      return state;
  }
};

export default commits;
