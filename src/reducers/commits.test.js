import commits from './commits';
import * as actions from '../actions/commits';

describe('commits reducer', () => {
  it('should handle SET_COMMITS', () => {
    const payload = [{
      commit: {
        message: 'Lorem ipsum dolor.',
        author: { name: 'Some Author' }
      }
    }, {
      commit: {
        message: 'Sit amet.',
        author: { name: 'Some Author' }
      }
    }];
    expect(
      commits({commits: [], visibleCommits: []}, {
        type: actions.SET_COMMITS,
        payload
      })
    ).toEqual({
      commits: payload,
      visibleCommits: payload
    });
  });

  it('should handle FILTER_COMMITS', () => {
    const initialState = {
      commits: [{
        commit: {
          message: 'Lorem ipsum dolor.',
          author: { name: 'Some Author' }
        }
      }, {
        commit: {
          message: 'Sit amet.',
          author: { name: 'Some Author' }
        }
      }],
      visibleCommits: []
    }
    const expectedState = {
      ...initialState,
      visibleCommits: [{
        commit: {
          message: 'Lorem ipsum dolor.',
          author: { name: 'Some Author' }
        }
      }]
    };

    expect(
      commits(initialState, {
        type: actions.FILTER_COMMITS,
        payload: 'ipsum'
      })
    ).toEqual(expectedState);
  });
});
