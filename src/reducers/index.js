import { combineReducers } from 'redux';
import commits from './commits';
import repos from './repos';

export default combineReducers({
  repos,
  commits
});
