import repos from './repos';
import * as actions from '../actions/repos';

describe('repos reducer', () => {
  it('should handle SET_REPOS', () => {
    const payload = [{
      title: 'Repository title',
      author: 'Some Author'
    }, {
      title: 'Another repository title',
      author: 'Other Author'
    }];
    expect(
      repos([], {
        type: actions.SET_REPOS,
        payload
      })
    ).toEqual(payload);
  });
});
