import reducer from './';

describe('redux store setup', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      repos: [],
      commits: {
        commits: [],
        visibleCommits: []
      }
    });
  });
});
