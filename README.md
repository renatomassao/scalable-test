# Task 1
The App fetches the data from the `facebook` github user, and displays the public repositories.  
## Things to improve
- Include field to search for users.
- Improve what is shown.
- Maybe set the user and repository selected on the state?

# Task 2, 3
The App fetches commits from the selected repository. And filter with the term that is set into the input.
## Things to improve
- Add more information for each commit, like the SHA.
- Add infinite scrolling.

# ES6+
- Spread operator `...`, to have an easier way to interact with Arrays and Objects, and creating new Objects while replacing some properties and concatenate Arrays.
- Arrow Functions `() => {}`, in the React components, since the arrow function already return if there is no `{}` it's pretty convenient to be used with map and filter.
- Classes `class` keyword, to be able to interact with the lifecycle of React components, like `ComponentWillMount` and stuff like thtat
- Also some of template literals to pass the styles to `styled-components` and if it was needed to interpolate strings.
